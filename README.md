Code to Rule
============
With apologies to [James Powell](https://github.com/dutc/code-or-die).

Code to Rule is a 4X game where the interface is an HTTP API.

(This documentation may be somewhat inexact.)

* `GET` `/`: The server, version, and actual URL routes
* `GET` `/ships`, `/factions`, `/systems`: List the thing
* `GET` `/ships/<sid>`, `/systems/<sysid>`: Get the specific thing
* `POST` `/ships/<sid>`: Tell a ship to do a thing:
    * `action=move`, `system=<sysid>`: Move to a system
    * `action=build`: Build a new ship
    * `action=idle`: Stop activity (loosing progress)
* EventSource `/events`: Get updates

Note that authentication is required (use the token as the password).

There is also a telnet interface at localhost:2323 for GMs.

Installation
============

Install libsodium, and then:

```
$ pipenv install
$ pipenv run python -m codetorule
```
