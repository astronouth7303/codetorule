import twisted.conch.manhole_tap

UNKEYED = {'make_faction', 'make_faction_token', 'make_system'}


class EngineWrapper:
    def __init__(self, game, fid):
        self.game = game
        self.key = game.make_faction_token(fid)

    def __getattr__(self, name):
        meth = getattr(self.game, name)
        if callable(meth) and name not in UNKEYED:
            return lambda *pargs, **kwargs: meth(self.key, *pargs, **kwargs)
        else:
            return meth


def make_manhole(username, password, globals):
    with open('/tmp/ctr.passwd', 'w') as ntf:
        ntf.write(f"{username}:{password}:::::\n")

    s = twisted.conch.manhole_tap.makeService({
        'telnetPort': "tcp:port=2323",
        'passwd': '/tmp/ctr.passwd',
        'namespace': globals,
        'sshPort': None,
    })

    return s
