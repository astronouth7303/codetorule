"""
Models for the game
"""

import attr
from typing import Optional, Union


class NotNowError(Exception):
    """
    Can't do that at this time
    """


@attr.s(auto_attribs=True)
class Faction:
    id: int
    name: str = ""


@attr.s(auto_attribs=True)
class System:
    id: int
    name: str = ""


@attr.s(auto_attribs=True)
class Lane:
    left: System
    right: System
    traveltime: float

    def other(self, system):
        if self.left == system:
            return self.right
        elif self.right == system:
            return self.left
        else:
            raise ValueError


@attr.s(auto_attribs=True)
class IdleStatus:
    # Stationary
    idle: System


@attr.s(auto_attribs=True)
class MovingStatus:
    # In-transit
    origin: System
    dest: System

    def cancel(self):
        raise NotNowError


@attr.s(auto_attribs=True)
class BuildingStatus:
    # In-transit
    building: System


@attr.s(auto_attribs=True)
class Ship:
    id: int
    faction: Faction
    status: Union[IdleStatus, MovingStatus]

    @property
    def system(self):
        if isinstance(self.status, IdleStatus):
            return self.status.idle
        elif isinstance(self.status, MovingStatus):
            return None
        elif isinstance(self.status, BuildingStatus):
            return self.status.building
