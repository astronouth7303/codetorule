import secrets
import attr
import pymacaroons
from twisted.internet import reactor

from .schema import *

def asdict(obj):
    return attr.asdict(obj)


def uniquify(seq):
    seen = set()
    for obj in seq:
        if id(obj) not in seen:
            seen.add(id(obj))
            yield obj


class EventQueue:
    def __init__(self):
        self.events = []
        self.callbacks = set()  # FIXME: Should this be weakref?

    def add_callback(self, func, resume_id=None):
        if resume_id is not None:
            from_id = int(resume_id) + 1
            for eid, event in enumerate(self.events[from_id:], from_id):
                try:
                    func(eid, *event)
                except Exception:
                    traceback.print_exc()
                    return
        self.callbacks.add(func)

    def add_event(self, tag, data):
        eid = len(self.events)
        self.events.append((tag, data))
        for func in self.callbacks.copy():
            try:
                func(eid, tag, data)
            except Exception:
                traceback.print_exc()
                self.callbacks.discard(func)


class PermissionError(Exception):
    """
    You do not have permission for this
    """


class BadKeyError(Exception):
    """
    The given key was invalid
    """


class NoLaneError(Exception):
    """
    There's no lane to get there
    """


class GameEngine:
    def __init__(self):
        self._factions = {}
        self._ships = {}
        self._systems = {}
        self._lanes = []
        self._queue = EventQueue()
        self._secret = secrets.token_bytes()

    def _parse_key(self, key):
        m = pymacaroons.Macaroon.deserialize(key)
        v = pymacaroons.Verifier()

        faction_id = int(m.identifier)
        v.satisfy_exact(f'faction:{faction_id}')

        try:
            if v.verify(m, self._secret):
                return self._factions[faction_id]
            else:
                raise BadKeyError
        except Exception:
            raise BadKeyError

    def event_subscribe(self, key, callback, resume_id=None):
        faction = self._parse_key(key)
        faction._queue.add_callback(callback, resume_id)

    def _factions_for_system(self, system):
            yield from (
                ship.faction
                for ship in self._ships.values()
                if ship.system == system
            )

    def _queues_for_event(self, tag, data):
        yield self._queue
        if tag in ('ship-new', 'ship-update'):
            # data is asdict[Ship]
            yield self._factions[data['faction']['id']]._queue
            system = [data['status'][k] for k in ('idle', 'building') if k in data['status']]
            if system:
                yield from (
                    f._queue
                    for f in self._factions_for_system(system[0]['id'])
                )
        elif tag in ('ship-departing', 'ship-arrived'):
            # data is {'ship': asdict[Ship], 'system': str}
            yield self._factions[data['ship']['faction']['id']]._queue
            yield from (
                f._queue
                for f in self._factions_for_system(data['system'])
            )

    def _add_event(self, tag, data):
        if not isinstance(data, (list, dict)):
            data = asdict(data)
        for q in uniquify(self._queues_for_event(tag, data)):
            q.add_event(tag, data)

    def list_factions(self, key):
        self._parse_key(key)
        for f in self._factions.values():
            yield asdict(f)

    def make_faction(self, name=""):
        if self._factions:
            fid = max(self._factions.keys()) + 1
        else:
            fid = 1

        f = Faction(id=fid, name=name)
        f._queue = EventQueue()
        self._factions[fid] = f
        return asdict(f)

    def make_faction_token(self, fid):
        assert fid in self._factions.keys()
        m = pymacaroons.Macaroon(
            identifier=str(fid),
            key=self._secret,
        )

        m.add_first_party_caveat(f'faction:{fid}')
        return m.serialize()

    def make_system(self, name=""):
        if self._systems:
            sid = max(self._systems.keys()) + 1
        else:
            sid = 1

        s = System(id=sid, name=name)
        self._systems[sid] = s
        return asdict(s)

    def list_systems(self, key):
        faction = self._parse_key(key)
        for sys in uniquify(s.system for s in self._ships.values() if s.faction == faction):
            if sys is not None:
                yield asdict(sys)

    def get_system(self, key, sysid):
        faction = self._parse_key(key)
        system = self._systems[sysid]
        if not any(s.system == system for s in self._ships.values() if s.faction == faction):
            raise PermissionError
        rv = asdict(system)
        rv['lanes'] = {
            l.other(system).id: l.traveltime
            for l in self._lanes
            if system in (l.left, l.right)
        }
        return rv

    def add_lane(self, leftid, rightid, traveltime):
        if leftid > rightid:
            rightid, leftid = leftid, rightid

        if any(l.left.id == leftid and l.right.id == rightid for l in self._lanes):
            return

        left = self._systems[leftid]
        right = self._systems[rightid]
        self._lanes.append(
            Lane(left=left, right=right, traveltime=traveltime)
        )

    def list_ships(self, key):
        faction = self._parse_key(key)
        for s in self._ships.values():
            if s.faction == faction:
                yield asdict(s)

    def get_ship(self, key, sid: int):
        faction = self._parse_key(key)
        ship = self._ships[sid]
        if faction != ship.faction:
            raise PermissionError
        return asdict(self._ships[sid])

    def add_ship(self, key, sysid: int):
        faction = self._parse_key(key)
        if self._ships:
            sid = max(self._ships.keys()) + 1
        else:
            sid = 1

        system = self._systems[sysid]

        s = Ship(
            id=sid,
            faction=faction,
            status=IdleStatus(idle=system),
        )
        self._ships[s.id] = s
        self._add_event('ship-new', asdict(s))
        return asdict(s)

    def _get_lane(self, left, right):
        if left.id > right.id:
            right, left = left, right
        try:
            return next(
                l
                for l in self._lanes
                if l.left == left and l.right == right
            )
        except StopIteration:
            raise NoLaneError

    def move_ship(self, key, sid, sysid):
        faction = self._parse_key(key)
        ship = self._ships[sid]

        if ship.faction != faction:
            raise PermissionError

        system = self._systems[sysid]

        if not isinstance(ship.status, IdleStatus):
            raise NotNowError
        old_system = ship.system

        lane = self._get_lane(old_system, system)

        self._add_event('ship-departing', {
            'ship': asdict(ship),
            'system': old_system.id,
        })

        ship.status = MovingStatus(origin=old_system, dest=system)

        def _finish_move():
            ship.status = IdleStatus(idle=system)
            self._add_event('ship-arrived', {
                'ship': asdict(ship),
                'system': system.id,
            })

        reactor.callLater(lane.traveltime, _finish_move)

    def build_ship(self, key, sid):
        faction = self._parse_key(key)
        parent = self._ships[sid]

        if parent.faction != faction:
            raise PermissionError

        buildtime = 10.0

        if not isinstance(parent.status, IdleStatus):
            raise NotNowError
        system = parent.system

        parent.status = BuildingStatus(building=system)

        self._add_event('ship-update', asdict(parent))

        def _finish_build():
            self.add_ship(key, system.id)
            parent.status = IdleStatus(idle=system)
            self._add_event('ship-update', asdict(parent))

        reactor.callLater(buildtime, _finish_build)

    def stop_ship(self, key, sid):
        faction = self._parse_key(key)
        ship = self._ships[sid]

        if ship.faction != faction:
            raise PermissionError

        old_status = ship.status

        if hasattr(old_status, 'cancel'):
            old_status.cancel()

        ship.status = IdleStatus(idle=ship.system)
        self._add_event('ship-update', asdict(ship))

    # Attack: Use twisted.internet.task.LoopingCall