import json
import tempfile
from functools import wraps

import klein
import mimeparse

import twisted.web.iweb
from twisted.web.server import NOT_DONE_YET

from . import __version__
from .engine import GameEngine, PermissionError, BadKeyError, NotNowError, NoLaneError
from .manhole import make_manhole, EngineWrapper


def fix_bytesdict(args):
    for k, v in args.items():
        k = k.decode('utf-8')
        if isinstance(v, bytes):
            yield k, v.decode('utf-8')
        elif isinstance(v, list):
            v = [
                i.decode('utf-8') if isinstance(i, bytes) else i
                for i in v
            ]
            if len(v) == 1:
                yield k, v[0]
            else:
                yield k, v
        else:
            yield k, v


def post_body(request: twisted.web.iweb.IRequest):
    """
    Will handle JSON/form handling
    """
    h = request.getHeader('Content-Type')
    if h is None:
        ct = st = params = None
    else:
        ct, st, params = mimeparse.parse_mime_type(h)

    if (ct, st) in (('application', 'x-www-form-urlencoded'), ('multipart', 'form-data')):
        return dict(fix_bytesdict(request.args))
    elif (ct, st) == ('application', 'json'):
        return json.loads(request.content.read())
    else:
        return dict(fix_bytesdict(request.args))  # I guess?


def json_response(request: twisted.web.iweb.IRequest, data, status=200):
    request.setResponseCode(status)
    request.setHeader('Content-Type', 'application/json')
    return json.dumps(data)


def text_response(request, data, status=200):
    request.setResponseCode(status)
    request.setHeader('Content-Type', 'text/plain; charset=utf-8')
    return data.encode('utf-8')


def empty_response(request, status=200):
    request.setResponseCode(status)
    return b''


def auth_required(meth):
    @wraps(meth)
    def wrapper(self, request, *pargs, **kwargs):
        if not request.getPassword():
            request.setHeader('WWW-Authenticate', 'Basic')
            return empty_response(request, 401)
        try:
            return meth(self, request, *pargs, **kwargs)
        except PermissionError:
            return text_response(request, "Don't have permissions to do that", 403)
        except BadKeyError:
            request.setHeader('WWW-Authenticate', 'Basic')
            return text_response(request, "Bad token", 401)
        except NotNowError:
            return text_response(request, "Can't do that with the current state", 401)
        except NoLaneError:
            return text_response(request, "There's no lane to that system", 401)
    return wrapper


class CtrApp:
    app = klein.Klein()

    def __init__(self):
        self.game = GameEngine()

        self.manhole = make_manhole('admin', 'admin', {
            'game': lambda fid: EngineWrapper(self.game, fid)
        })        

    @app.route('/', methods=['GET'])
    @auth_required
    def index(self, request):
        return text_response(request, 
            f'codetorule/{__version__}\n\n' +
            '\n'.join(
                f'{method} {rule.rule}'
                for rule in self.app.url_map.iter_rules()
                for method in rule.methods
                if method != 'HEAD'
            )
        )

    @app.route('/events', methods=['GET'])
    @auth_required
    def event_stream(self, request):
        request.setResponseCode(200)
        request.setHeader('Content-Type', 'text/event-stream')

        last_event = request.getHeader('Last-Event-ID')
        try:
            last_event = int(last_event)
        except (ValueError, TypeError):
            pass

        def send_event(id, tag, data):
            request.write(f"id: {id}\n".encode('utf-8'))
            request.write(f"event: {tag}\n".encode('utf-8'))
            request.write(f"data: {json.dumps(data)}\n".encode('utf-8'))
            request.write(b"\n")

        self.game.event_subscribe(request.getPassword(), send_event, last_event)

        return NOT_DONE_YET

    @app.route('/factions/', methods=['GET'])
    @auth_required
    def list_factions(self, request):
        return json_response(request, [
            faction
            for faction in self.game.list_factions(request.getPassword())
        ])

    @app.route('/systems/', methods=['GET'])
    @auth_required
    def list_systems(self, request):
        return json_response(request, [
            faction
            for faction in self.game.list_systems(request.getPassword())
        ])

    @app.route('/systems/<int:sysid>', methods=['GET'])
    @auth_required
    def get_system(self, request, sysid):
        return json_response(request, self.game.get_system(request.getPassword(), sysid))

    @app.route('/ships/', methods=['GET'])
    @auth_required
    def list_ships(self, request):
        return json_response(request, [
            ship
            for ship in self.game.list_ships(request.getPassword())
        ])

    @app.route('/ships/<int:sid>', methods=['GET'])
    @auth_required
    def get_ship(self, request, sid):
        ship = self.game.get_ship(request.getPassword(), sid)
        return json_response(request, ship)

    @app.route('/ships/<int:sid>', methods=['POST'])
    @auth_required
    def mod_ship(self, request, sid):
        body = post_body(request)

        if 'action' not in body:
            return text_response(request, "No action given", 400)

        if body['action'] == 'move':
            self.game.move_ship(request.getPassword(), sid, int(body['system']))
            return empty_response(request, 202)
        elif body['action'] == 'build':
            self.game.build_ship(request.getPassword(), sid)
            return empty_response(request, 202)
        elif body['action'] == 'idle':
            self.game.stop_ship(request.getPassword(), sid)
            return empty_response(request, 200)
        else:
            return text_response(request, "Unknown action", 400)
