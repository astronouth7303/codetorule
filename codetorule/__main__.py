import sys
from twisted.internet import endpoints, reactor
from twisted.web.server import Site
from codetorule.server import CtrApp
from codetorule.gen import gen_simple, gen_large
from twisted.python import log

server = CtrApp()

keys = gen_large(server.game)
for f, k in keys.items():
    print(f"faction {f}: {k}")

log.startLogging(sys.stderr)

endpoint = endpoints.serverFromString(reactor, "tcp:port=8080:interface=localhost")
endpoint.listen(Site(server.app.resource()))
server.manhole.startService()
reactor.run()
