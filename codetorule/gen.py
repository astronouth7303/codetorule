"""
Code to do initial generation.
"""

import collections
import random


def gen_simple(game):
    """
    Make a trivial two-faction, two-system game
    """
    def mkfaction(name, home):
        f = game.make_faction(name)
        key = game.make_faction_token(f['id'])
        sys = game.make_system(home)
        ship = game.add_ship(key, sys['id'])

        return f, key, sys, ship

    _, red_key, _, _ = mkfaction('red', 'redinium')
    _, blu_key, _, _ = mkfaction('blu', 'blurion')

    game.add_lane(1, 2, 5.0)

    return red_key, blu_key


def gen_large(game, factions=4, systems=1000, connections=3):
    def mkfaction():
        f = game.make_faction()
        key = game.make_faction_token(f['id'])
        sys = game.make_system()
        ship = game.add_ship(key, sys['id'])

        return f['id'], key

    keys = dict(mkfaction() for _ in range(factions))

    for _ in range(systems):
        game.make_system()

    rand = random.SystemRandom()

    sysids = list(game._systems.keys())
    lanecount = collections.Counter({id: 0 for id in sysids})

    while any(c < connections for c in lanecount.values()):
        for s in sysids:
            if lanecount[s] < connections:
                other = rand.choice(sysids)
                if other == s:
                    continue
                game.add_lane(s, other, 5.0)
                lanecount[s] += 1
                lanecount[other] += 1

    print(f"Max lanes: {max(lanecount.values())}")
    print(f"Min lanes: {min(lanecount.values())}")

    return keys
